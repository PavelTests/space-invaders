//звуки
const backgroundSound = document.getElementById('background');
const endgameSound1 = document.getElementById('endgame1');
const shotSound = document.getElementById('shot');
const destroyedLifeSound = document.getElementById('destroyedLifeSound');

const canvas = document.getElementById('game');
const context = canvas.getContext('2d');

//размеры сетки и счетчик кадров
const grid = 8;
let frameСounter = 0;

//настройки врагов
const numberOfEnemiesPerLine = 6;
const linesWithEnemies = 3;
const enemiesStartLine = 14;
const enemiesGap = 3;
let enemiesSequence = {};
const enemiesCost = 60;

//настойки каждого раунда
const roundSettings = [{
  colorEnemies: 'purple',
  enemyShift: 3,
  enemiesSpeed: 10
}, {
  colorEnemies: 'blue',
  enemyShift: 3,
  enemiesSpeed: 5
}, {
  colorEnemies: 'orange',
  enemyShift: 4,
  enemiesSpeed: 2
}]

//настройки шаттла
const spaceShipStartLine = 76;
let spaceShip = {};

let lasersSequence = {};
//нумеруем каждый лазер для возможности удаления
let laserId = 0;

const colors = {
  'spaceShip': 'green',
  'laser': 'red',
  'lifes': 'red'
};


//направление движения врагов -- вправо
let directionEnemies = 1;
//говорим, то сразу сдвигать вниз врагов не надо
let isEnemiesGotDown = true;

//настройки пользователя
let userScore = 0;//тек. очки
let totalScore = +localStorage.getItem('totalScore');
const userTotalLifes = 3;//всего жизней
let userCurrLifes = userTotalLifes;
let roundCounter = 0;//тек. раунд


// отслеживаем анимацию для возможности ее отмены
let rAF = null;
let gameOver = false;
//флаг самой первой игры
let startFirstGame = true;

//игр. поле 50х80 для хранения выстрелов
const playfield = [];


//-----------------------------------------------
//управление игрой
function generateNewGame() {
  endgameSound1.pause();

  //очистка игр. поля от выстрелов
  for (let row = -2; row < 80; row++) {
    playfield[row] = [];
    for (let col = 0; col < 50; col++) {
      playfield[row][col] = 0;
    }
  }
  //направление движения врагов -- вправо
  directionEnemies = 1;
  //говорим, то сразу сдвигать вниз врагов не надо
  isEnemiesGotDown = true;

  lasersSequence = {};
  enemiesSequence = generateEnemies();
  spaceShip = generateSpaceShip();
}

function generateGameOver() {
  gameOver = true;

  //звук окончания игры
  backgroundSound.pause();
  endgameSound1.volume = 0.3;
  endgameSound1.currentTime = 0;
  endgameSound1.play();

  //если новый рекорд
  if (userScore > totalScore) {
    totalScore = userScore;
    localStorage.setItem('totalScore', userScore);
  }
  userScore = 0;

  context.fillStyle = 'black';
  context.globalAlpha = 0.75;
  context.fillRect(0, canvas.height / 2 - 30, canvas.width, 60);

  context.globalAlpha = 1;
  context.fillStyle = 'white';
  context.font = '36px Verdana';
  context.textAlign = 'center';
  context.textBaseline = 'middle';
  context.fillText('GAME OVER!', canvas.width / 2, canvas.height / 2);
  context.font = '12px Verdana';
  context.fillText('press space', canvas.width / 2, canvas.height / 2 + 30);
}

function generateNewRound() {
  //новый раунд
  roundCounter++;
  showNewRound();
  cancelAnimationFrame(rAF);

  setTimeout(() => {
    generateNewGame();
    rAF = requestAnimationFrame(loop);
  }, 2000);
}

function stopGame() {
  cancelAnimationFrame(rAF);
  userCurrLifes--;

  //если жизни остались, то продолжаем
  if (userCurrLifes > 0) {
    //звук врезания врага в границу
    destroyedLifeSound.volume = 0.4;
    destroyedLifeSound.currentTime = 0;
    destroyedLifeSound.play();

    setTimeout(() => {
      generateNewGame();
      rAF = requestAnimationFrame(loop);
    }, 1000);
  }
  else generateGameOver();
}

function showNewRound() {
  context.fillStyle = 'black';
  context.globalAlpha = 0.75;
  context.fillRect(0, canvas.height / 2 - 30, canvas.width, 60);

  context.globalAlpha = 1;
  context.fillStyle = 'white';
  context.font = '36px Verdana';
  context.textAlign = 'center';
  context.textBaseline = 'middle';
  context.fillText('NEW ROUND!', canvas.width / 2, canvas.height / 2);
}

function showStartFirstGame() {
  context.fillStyle = 'black';
  context.globalAlpha = 0.75;
  context.fillRect(0, canvas.height / 2 - 30, canvas.width, 60);

  context.globalAlpha = 1;
  context.fillStyle = 'white';
  context.font = '36px Verdana';
  context.textAlign = 'center';
  context.textBaseline = 'middle';
  context.fillText('START GAME', canvas.width / 2, canvas.height / 2);
  context.font = '12px Verdana';
  context.fillText('press space', canvas.width / 2, canvas.height / 2 + 30);
}

function showScore() {
  context.globalAlpha = 1;
  context.fillStyle = 'white';
  context.font = '14px Verdana';
  context.textAlign = 'left';
  context.textBaseline = 'middle';
  context.fillText(`Rec: ${totalScore}   Score: ${userScore}`, canvas.width / 2, 10);
}

function getRoundSettings() {
  //т.к. настройки только для первых 3х раундов
  if (roundCounter > 2) return roundSettings[2];
  else return roundSettings[roundCounter];
}

//-----------------------------------------------
//генерация объектов
function generateLaser() {
  //от размеров лазера зависит moveLasers() 
  laserId++;
  const laser = [[1], [1]];
  const cellCol = spaceShip.col + Math.floor(spaceShip.matrix[0].length / 2);
  const cellRow = spaceShip.row;

  //отмечаем лазер на игровом поле
  for (let row = 0; row < laser.length; row++) {
    for (let col = 0; col < laser[row].length; col++) {
      playfield[row + cellRow][col + cellCol] = laserId;
    }
  }

  return {
    matrix: laser,
    col: cellCol,
    row: cellRow,
    ID: laserId
  }
}

function generateSpaceShip() {
  const ship = [
    [0, 0, 1, 0, 0],
    [0, 1, 1, 1, 0],
    [1, 1, 1, 1, 1],
    [1, 1, 1, 1, 1]];
  const shipWidth = ship[0].length;

  return {
    matrix: ship,
    row: spaceShipStartLine,
    col: Math.floor(playfield[0].length / 2 - shipWidth / 2)
  }
}

function generateEnemies() {
  const enemy = [
    [1, 1, 1, 1],
    [1, 0, 0, 1],
    [1, 1, 1, 1],
    [1, 0, 0, 1]];
  const enemyHeight = enemy.length;
  const enemyWidth = enemy[0].length;


  let enemiesSequence = {};
  for (let line = 0; line < linesWithEnemies; line++) {
    for (let enem = 0; enem < numberOfEnemiesPerLine; enem++) {
      let key = '' + line + enem;//01 , 02 ...
      enemiesSequence[key] = {
        matrix: enemy,
        row: enemiesStartLine + (enemyHeight + enemiesGap) * line,
        col: enem * (enemyWidth + enemiesGap)
      }
    }
  }
  return enemiesSequence;
}


//-----------------------------------------------
//сдвиги объектов
function isValidMoveShip(matrix, cellCol) {
  for (let row = 0; row < matrix.length; row++) {
    for (let col = 0; col < matrix[row].length; col++) {
      if (matrix[row][col] && (
        // вышли за пределы игр. поля
        cellCol + col < 0 ||
        cellCol + col >= playfield[0].length)
      ) {
        return false;
      }
    }
  }
  return true;
}

function moveLasers() {
  for (let [key, laser] of Object.entries(lasersSequence)) {
    const newRow = laser.row - 1;

    //смещаем выстрел в игровом поле 
    //0  1
    //1->1 
    //1  0
    playfield[newRow][laser.col] = playfield[laser.row][laser.col];
    playfield[laser.row + 1][laser.col] = playfield[laser.row + 2][laser.col];

    //если вышли за пределы игрового поля
    if (newRow === -2) delete lasersSequence[key];
    else laser.row = newRow;
  }
}

function moveEnemies() {
  let minCol = 1e8, maxCol = -1;
  //определяем границы(лев. и прав. столбцы) для enemiesSequence
  Object.values(enemiesSequence).forEach(enemy => {
    minCol = Math.min(minCol, enemy.col);
    maxCol = Math.max(maxCol, enemy.col + enemy.matrix[0].length);
  })

  //если враги на одной из границ и еще не опускались вниз
  if ((minCol === 0 || maxCol === playfield[0].length) && !isEnemiesGotDown) {
    isEnemiesGotDown = true;
    for (enemy of Object.values(enemiesSequence)) {
      //сдвиг врага вниз
      const newRow = enemy.row + getRoundSettings().enemyShift;
      //если при сдвиге врагов вниз выходим за нижнюю границу
      const bottomBound = newRow + enemy.matrix.length;
      const spaceShipHeight = spaceShip.matrix.length;
      if (bottomBound > playfield.length - spaceShipHeight - 1) {
        //сместим одного врага для наглядности за границу
        enemy.row = newRow;
        //остановка игры
        stopGame();
        return;
      }

      enemy.row = newRow;
    };
    //меняем направление
    directionEnemies = -directionEnemies;
  }
  //иначе просто сдвигаем врагов в заданном направлении
  else {
    isEnemiesGotDown = false;
    Object.values(enemiesSequence).forEach(enemy => enemy.col += directionEnemies);
  }
}


//-----------------------------------------------
//обработка столкновений лазеров и врагов
function enemyСollisionIndex(enemy) {
  const cellRow = enemy.row;
  const cellCol = enemy.col;

  for (let row = 0; row < enemy.matrix.length; row++) {
    for (let col = 0; col < enemy.matrix[row].length; col++) {
      //если единица в матрице и в игр. поле => столкновение
      if (enemy.matrix[row][col] && playfield[cellRow + row][cellCol + col]) {
        //индекс столкновения
        return [cellRow + row, cellCol + col];
      }
    }
  }
  return null;
}

function deleteDeadEnemiesAndLaser() {
  for (let [key, enemy] of Object.entries(enemiesSequence)) {
    let indexСollision = enemyСollisionIndex(enemy);
    //если попали
    if (indexСollision) {
      delete enemiesSequence[key];
      userScore += enemiesCost;

      //удаление выстрела в lasersSequence и игровом поле
      const [rowCollis, colCollis] = indexСollision;
      const laserID = playfield[rowCollis][colCollis];
      delete lasersSequence[laserID];
      playfield[rowCollis][colCollis] = 0;
      playfield[rowCollis + 1][colCollis] = 0;
      playfield[rowCollis - 1][colCollis] = 0;
    }
  }

}


//-----------------------------------------------
//отрисовка объектов
function drawLasers() {
  context.fillStyle = colors['laser'];
  Object.values(lasersSequence).forEach(laser => {
    for (let row = 0; row < laser.matrix.length; row++) for (let row = 0; row < laser.matrix.length; row++) {
      for (let col = 0; col < laser.matrix[row].length; col++) {
        //если единица в матрице
        if (laser.matrix[row][col]) {
          context.fillRect((laser.col + col) * grid, (laser.row + row) * grid, grid, grid);
        }
      }
    }
  })
}

function drawSpaceShip() {
  context.fillStyle = colors['spaceShip'];
  for (let row = 0; row < spaceShip.matrix.length; row++) {
    for (let col = 0; col < spaceShip.matrix[row].length; col++) {
      //если единица в матрице
      if (spaceShip.matrix[row][col]) {
        context.fillRect((spaceShip.col + col) * grid, (spaceShip.row + row) * grid, grid, grid);
      }
    }
  }
}

function drawEnemiesSequence() {
  const color = getRoundSettings().colorEnemies;
  context.fillStyle = color;

  Object.values(enemiesSequence).forEach(enemy => {
    for (let row = 0; row < enemy.matrix.length; row++) {
      for (let col = 0; col < enemy.matrix[row].length; col++) {
        //если единица в матрице
        if (enemy.matrix[row][col]) {
          context.fillRect((enemy.col + col) * grid, (enemy.row + row) * grid, grid, grid);
        }
      }
    }
  })
}

function drawLifes() {
  const lifeMatrix = [
    [0, 1, 0, 1, 0],
    [1, 1, 1, 1, 1],
    [1, 1, 1, 1, 1],
    [0, 1, 1, 1, 0],
    [0, 0, 1, 0, 0]
  ];
  const lifeMatrWidth = lifeMatrix[0].length;
  let lifeColStart = 0;
  const lifeRowStart = 0;
  const lifeColGap = 2;

  context.fillStyle = colors['lifes'];
  for (let life = 0; life < userCurrLifes; life++) {
    //отрисовка матрицы жизни
    for (let row = 0; row < lifeMatrix.length; row++) {
      for (let col = 0; col < lifeMatrix[row].length; col++) {
        //если единица в матрице
        if (lifeMatrix[row][col]) {
          context.fillRect((lifeColStart + col) * grid, (lifeRowStart + row) * grid, grid, grid);
        }
      }
    }
    //сдвиг для отрисовка след. жизни
    lifeColStart += lifeMatrWidth + lifeColGap;
  }
}

// игровой цикл
function loop() {
  rAF = requestAnimationFrame(loop);
  context.clearRect(0, 0, canvas.width, canvas.height);

  moveLasers();

  //соблюдение скорости врагов
  const enemSpeed = getRoundSettings().enemiesSpeed;
  if (++frameСounter > enemSpeed) {
    frameСounter = 0;
    moveEnemies();
  }

  deleteDeadEnemiesAndLaser();

  //отрисовка активных лазеров
  drawLasers();

  //отрисовка шаттла
  drawSpaceShip();

  // отрисовка активных врагов
  drawEnemiesSequence();

  //отрисовка оставшихся жизней
  drawLifes();

  showScore();

  //отрисовка границы
  context.fillStyle = 'purple';
  context.fillRect(0, grid * 75, canvas.width, 2);

  const livingEnemies = Object.keys(enemiesSequence).length;
  //не осталось живых врагов
  if (livingEnemies === 0) {
    generateNewRound();
  }
}

// прослушивание нажатий
document.addEventListener('keydown', function (e) {
  //запуск самой первой игры
  if (e.code === 'Space' && startFirstGame) {
    startFirstGame = false;
    backgroundSound.play();
    backgroundSound.volume = 0.2;
    // начать игру
    generateNewGame();
    rAF = requestAnimationFrame(loop);
  }

  //запуск новой игры после gameOver
  if (e.code === 'Space' && gameOver) {
    gameOver = false;
    //обновление жизней
    userCurrLifes = userTotalLifes;
    //обнуление раундов
    roundCounter = 0;

    backgroundSound.play();
    generateNewGame();
    rAF = requestAnimationFrame(loop);
    return;
  };

  // сдвиг шаттла вправо и влево
  if (e.code === 'ArrowLeft' || e.code === 'ArrowRight') {
    const col = e.code === 'ArrowLeft'
      ? spaceShip.col - 1
      : spaceShip.col + 1;

    if (isValidMoveShip(spaceShip.matrix, col)) {
      spaceShip.col = col;
    }
  }

  //генерация выстрелов
  if (e.code === 'Space' && !e.repeat) {
    const laser = generateLaser();
    lasersSequence[laser.ID] = laser;

    //звуки выстрела
    shotSound.volume = 0.3;
    shotSound.currentTime = 0;
    shotSound.play();
  }
});

//показываем в самом начале
showStartFirstGame();